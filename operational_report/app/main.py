"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Mrunalini Dhapodkar"


import json
import logging
from json import JSONDecodeError
import pymysql
from flask import Flask
from flask import request

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from flask_cors import CORS
from data import constants

config_file = '/app/config/dev.json'

config = XprConfigParser(config_file)
logger = XprLogger("operational_report", level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    CORS(flask_app)
    return flask_app


app = create_app()


@app.route('/')
def hello_world():
    """
    Send response to Hello World
    """

    logger.info("Received request from {}".format(request.remote_addr))
    try:
        logger.info("Processing the request")
        cfg_fs = open(config_file, 'r')
        config = json.load(cfg_fs)
        project_name = config["project_name"]
        logger.info("Request Processing Done")
        logger.info("Sending Response to {}".format(request.remote_addr))
        return '<html><body><b>Hello World from {}!</b></body></html>'.format(
            project_name
        )
    except (FileNotFoundError, JSONDecodeError):
        logger.error("Request Processing Failed")
        logger.info("Sending Default Response")
        return '<html><body><b>Hello World!</b></body></html>'


@app.route('/get_report', methods=['GET'])
def group_by_query():
    """
    Send report based on the stored procedure called
    :return: operational report
    """
    report_range = {}

    cursor = database_connector.cursor()
    # cursor.execute(f"select name from mysql.proc where db='doe';")
    #
    # for procname in cursor.fetchall():
    #     proc_name.append(procname['name'])

    if request.args and "query_json" in request.args:
        query_json = request.args.get("query_json")
        cursor.execute(f"call {query_json}()")
        for result in cursor.fetchall():
            proc_result = list(result.values())
            report_range.update({proc_result[0]: proc_result[1]})

    else:
        result = {"error": "Not a valid report"}
        return result

    result = {
        "report_name": query_json,
        "data": report_range
    }
    return result


if __name__ == '__main__':
    database_connector = pymysql.connect(
        host="172.16.6.56",
        port=32612,
        user=config[constants.MYSQL_SECTION][constants.USER],
        password=config[constants.MYSQL_SECTION][constants.PWD],
        database=config[constants.MYSQL_SECTION][constants.DB],
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )
    app.run(host="0.0.0.0", port=5050)
