"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import pymysql

__author__ = "### Author ###"

logger = XprLogger("generate_data_sheets",level=logging.INFO)


class GenerateDataSheets(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, experiment_id, database_ip, database_port):
        super().__init__(name="GenerateDataSheets")
        """ Initialize all the required constansts and data her """
        self.experiment_id = experiment_id
        self.database_ip = database_ip
        self.database_port = database_port

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            # 5. pull rejected participants into csv
            # create connection
            connection = pymysql.connect(host=self.database_ip,
                                         port=int(self.database_port),
                                         user='root',
                                         password='abz00ba1nc',
                                         db='doe')
            logger.info("Connected to database")

            # Create cursor
            cursor = connection.cursor()

            # Execute Query
            cursor.execute("SELECT id, covid19_positive, age, hypertension_present, diabetes_present,\
                              rejection_reason from participants_" + self.experiment_id +
                                " WHERE rejection_reason is not null;")
            rows = cursor.fetchall()
            df = pd.DataFrame(rows)
            logger.info("Fetched participant data from database: {} participants".format(df.shape[0]))
            report_status = {
                "status": {"status": "Fetched participant data from database"},
                "metric": {"number_of_participants": df.shape[0]}
            }
            self.send_metrics(report_status)

            df.rename(
                columns={0: "id", 1: "covid19_positive", 2: "age", 3: "hypertension_present", 4: "diabetes_present",
                         5: "rejection_reason"}, inplace=True)
            df.to_csv("/data/rejected.csv", index=False)

            cursor.close()
            logger.info("Generated rejected applicants list. Number of rejections:{}".format(df.shape[0]))
            report_status = {
                "status": {"status": "Generated rejected applicants list"},
                "metric": {"number_of_rejections": df.shape[0]}
            }
            self.send_metrics(report_status)

            # 6. create data collection sheet for accepted participants
            # Create cursor
            cursor = connection.cursor()

            # Execute Query
            cursor.execute("SELECT id, '' as date, '' as temperature, '' as bp, '' as covid19_positive, \
                           'Arm' as vaccine_site from participants_" + self.experiment_id + " where `group` in \
                           (1,3,5,7,9,11,13,15) \
                            AND rejection_reason is null\
                           UNION\
                            SELECT id, '' as date, '' as temperature, '' as bp, '' as covid19_positive, \
                            'Thigh' as vaccine_site from participants_" + self.experiment_id + " where `group` in \
                            (2,4,6,8,10,12,14,16)\
                            AND rejection_reason is null;")
            rows = cursor.fetchall()
            df = pd.DataFrame(rows)

            df.rename(columns={0: "id", 1: "date", 2: "temperature", 3: "bp", 4: "covid19_positive",
                               5: "vaccine_site"}, inplace=True)
            df.to_csv("/data/data_collection_sheet.csv", index=False)

            cursor.close()

            connection.close()
            logger.info("Generated data collection sheet. Number of particpants: {}".format(df.shape[0]))
            report_status = {
                "status": {"status": "Generated data collection sheet"},
                "metric": {"number_of_particpants": df.shape[0]}
            }
            self.send_metrics(report_status)

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self, report_status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    if len(sys.argv) < 5:
        print("Usage: generate_data_sheets <run_id> <experiment_id> <database_ip> <database_port>")
        exit(-1)
    data_prep = GenerateDataSheets(sys.argv[2], sys.argv[3], sys.argv[4])
    data_prep.start(run_name=sys.argv[1])
