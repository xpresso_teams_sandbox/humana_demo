"""
This is the implementation of data preparation for sklearn
"""
import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
from sqlalchemy import create_engine
import math

__author__ = "### Author ###"

logger = XprLogger("allocate_groups",level=logging.INFO)

LESS = "<"
MORE = ">="


class AllocateGroups(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, experiment_id, filename, database_ip, database_port):
        super().__init__(name="AllocateGroups")
        """ Initialize all the required constansts and data her """
        self.experiment_id = experiment_id
        self.filename = filename
        self.database_ip  = database_ip
        self.database_port = database_port

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            # 1. load data
            df = pd.read_csv(self.filename)
            logger.info("Loaded data successfully: {} participants loaded", df.shape[0])
            report_status = {
                "status": {"status": "Loaded participant data successfully"},
                "metric": {"number_of_participants": df.shape[0]}
            }
            self.send_metrics(report_status)


            # 2. form groups
            # gender - 2 levels
            # age - (20-71) - 6 levels (20-30, 30-40, 40-50, 50-60, 60-70, 70+)
            # weight (70 - 290) - 5 levels (<100, 100-150, 150-200, 200-250, 250+)
            # treatment factor (site)- 2 levels (arm / thigh)
            # hence 120 possible groups - reduce to 16 (2 each)
            df["group"] = 0
            '''
            M, < 45, <180, arm
            M, < 45, <180, thigh
            M, < 45, >=180, arm
            M, < 45, >=180, arm
            M, >= 45, <180, arm
            M, >= 45, <180, thigh
            M, >= 45, >=180, arm
            M, >= 45, >=180, arm
            F, < 45, <180, arm
            F, < 45, <180, thigh
            F, < 45, >=180, arm
            F, < 45, >=180, arm
            F, >= 45, <180, arm
            F, >= 45, <180, thigh
            F, >= 45, >=180, arm
            F, >= 45, >=180, arm
            '''
            self.allocate_group(df, "M", LESS, 45, LESS, 180, 1)
            self.allocate_group(df, "M", LESS, 45, MORE, 180, 3)
            self.allocate_group(df, "M", MORE, 45, LESS, 180, 5)
            self.allocate_group(df, "M", MORE, 45, MORE, 180, 7)
            self.allocate_group(df, "F", LESS, 45, LESS, 180, 9)
            self.allocate_group(df, "F", LESS, 45, MORE, 180, 11)
            self.allocate_group(df, "F", MORE, 45, LESS, 180, 13)
            self.allocate_group(df, "F", MORE, 45, MORE, 180, 15)
            logger.info("Allocated groups successfully")
            report_status = {
                "status": {"status": "Allocated groups successfully"}
            }
            self.send_metrics(report_status)

            # 3. update rejection reason
            # participants with covid19_positive = 'N' are not included
            # participants above 65 and with hypertension or diabetes are excluded
            df.loc[df["covid19_positive"] == 'No', 'group'] = 0
            df.loc[df["covid19_positive"] == 'No', 'rejection_reason'] = "Failed inclusion criteria"
            df.loc[
                (df["age"] > 60) & ((df["hypertension_present"] == 'Yes') | (df["diabetes_present"] == 'Yes')), 'group'] = 0
            df.loc[(df["age"] > 60) & ((df["hypertension_present"] == 'Yes') | (df["diabetes_present"] == 'Yes')),
                   'rejection_reason'] = "Met exclusion criteria"
            logger.info("Updated rejected participant info")
            report_status = {
                "status": {"status": "Updated rejected participant info"},
            }
            self.send_metrics(report_status)

            # 4. push data into databasep
            engine = create_engine("mysql+pymysql://{user}:{pw}@{ip}:{port}/{db}"
                                   .format(user="root",
                                           pw="abz00ba1nc",
                                           ip=self.database_ip,
                                           port=self.database_port,
                                           db="doe"))
            # Insert whole DataFrame into MySQL
            df.to_sql('participants_' + self.experiment_id, con=engine, if_exists='replace', chunksize=1000, index=False)
            logger.info("Stored participant information in database")
            report_status = {
                "status": {"status": "Stored participant information in database"},
            }
            self.send_metrics(report_status)


        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self, report_status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()

    def allocate_group(self, data, gender, age_operator, age_limit, weight_operator, weight_limit, group_id):
        # find rows of df corresponding to condition
        # allocate half the rows to group group_id
        # allocate the other half to group group_id +1
        subset = None
        if age_operator == LESS and weight_operator == LESS:
            subset = (data["gender"] == gender) & (data["age"] < age_limit) & (data["weight"] < weight_limit)
        elif age_operator == LESS and weight_operator == MORE:
            subset = (data["gender"] == gender) & (data["age"] < age_limit) & (data["weight"] >= weight_limit)
        elif age_operator == MORE and weight_operator == LESS:
            subset = (data["gender"] == gender) & (data["age"] >= age_limit) & (data["weight"] < weight_limit)
        elif age_operator == MORE and weight_operator == MORE:
            subset = (data["gender"] == gender) & (data["age"] >= age_limit) & (data["weight"] >= weight_limit)

        # set 45% of the rows where subset value is True to group_id, 45% to group_id + 1 and the rest to group_id + 2
        '''
        count = 0
        for i, value in subset.items():
            if value:
                if count % 2 == 0:
                    data.loc[i, "group"] = group_id
                else:
                    data.loc[i, "group"] = group_id + 1
                count = count + 1
        '''
        num_rows = subset.shape[0]
        valid_group_size = math.floor(0.45 * num_rows)
        for i, value in subset.items():
            if value:
                if i < valid_group_size:
                    data.loc[i, "group"] = group_id
                elif i < 2 * valid_group_size:
                    data.loc[i, "group"] = group_id + 1
                else:
                    data.loc[i, "group"] = group_id + 2


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    if len(sys.argv) < 6:
        print("Usage: allocate_groups <run_id> <experiment_id> <participant_data_file_path> <database_ip> <database_port>")
        exit (-1)

    data_prep = AllocateGroups(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    data_prep.start(run_name=sys.argv[1])
