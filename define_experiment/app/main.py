"""
    This is flask app is used to host an experiment definition
    web service for design of experiments project.
"""

__author__ = "Shlok Chaudhari"


import utils
import logging
import pymysql
import constants
import sqlalchemy as db
from flask import Flask, request
from xpresso.ai.core.logging.xpr_log import XprLogger

logger = XprLogger(constants.COMPONENT_NAME, level=logging.INFO)


def create_connection():
    """
        Creates engine that establishes connection with database component

    Returns:
         SQL connection object
    """
    template_uri = "mysql+pymysql://{user}:{pw}@{ip}:{port}/{db}"
    uri = template_uri.format(
        user=app.config[constants.DB_USERNAME_KEY],
        pw=app.config[constants.DB_PASSWORD_KEY],
        ip=app.config[constants.DB_HOST_KEY],
        port=app.config[constants.DB_PORT_KEY],
        db=app.config[constants.DB_DATABASE_KEY]
    )
    sqlEngine = db.create_engine(uri)
    dbConnection = sqlEngine.connect()
    return dbConnection, sqlEngine


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route(f"/{constants.REQUEST_URL}", methods=[constants.REQUEST_METHOD])
def define_experiment():
    """
        Creates entries for a new experiment in MySQL db
    """
    response = "Your experiment has been successfully created. " \
               "The experiment ID is {}"
    try:
        log_message = 'Received request for define_experiment from {}'
        print(log_message.format(request.remote_addr))

        input_request = utils.check_incoming_request()
        if constants.REQUEST_INPUT_KEY not in input_request:
            response = {"message": "Input data not provided"}, 400

        connection, sqlEngine = create_connection()
        utils.check_request_json_format(
            sqlEngine, input_request[constants.REQUEST_INPUT_KEY]
        )
        factors_list = input_request[constants.REQUEST_INPUT_KEY].pop(
            constants.DOE_FACTORS_TABLE
        )

        experiment_details = input_request[constants.REQUEST_INPUT_KEY]
        utils.insert_record_in_table(
            connection, constants.DOE_EXPERIMENTS_TABLE, experiment_details
        )

        experiment_id = utils.get_experiment_id(connection, sqlEngine)

        factors_list_with_exp_id = [
            factor_details.update({constants.COL_EXPERIMENT_ID: experiment_id})
            for factor_details in factors_list
        ]

        for factor_details in factors_list:
            if factor_details[constants.COL_BLOCKING_FACTOR] ==\
                    constants.STR_TRUE:
                factor_details[constants.COL_BLOCKING_FACTOR] = 1
            elif factor_details[constants.COL_BLOCKING_FACTOR] ==\
                    constants.STR_FALSE:
                factor_details[constants.COL_BLOCKING_FACTOR] = 0
            else:
                raise ValueError
        factors_list_with_exp_id = [
            factor_details for factor_details in factors_list
        ]
        for factor_details in factors_list_with_exp_id:
            utils.insert_record_in_table(
                connection, constants.DOE_FACTORS_TABLE, factor_details
            )

        response = {"message": response.format(experiment_id)}, 200

        log_message = 'Sending response for define_experiment to {}'
        print(log_message.format(request.remote_addr))
    except (Exception, pymysql.err.InternalError) as e:
        message = "Defining experiment failed. Here's the exception: {}"
        print(message.format(e))
        response = {"message": message.format(e)}, 400
    finally:
        print('Exiting with response {}'.format(response))
        return response


if __name__ == '__main__':
    """
        File that runs a flask API web service to automatically
        create new experiments in DOE database
    """
    app.config[constants.DB_USERNAME_KEY] = 'root'
    app.config[constants.DB_PASSWORD_KEY] = 'abz00ba1nc'
    app.config[constants.DB_HOST_KEY] = '172.16.6.51'
    app.config[constants.DB_PORT_KEY] = '32612'
    app.config[constants.DB_DATABASE_KEY] = 'doe'
    app.run(host="0.0.0.0", port=5000)
