"""
    Helper methods for flask app
"""

__author__ = "Shlok Chaudhari"


import json
import logging
import functools
import constants
import pandas as pd
import sqlalchemy as db
from flask import request
from xpresso.ai.core.logging.xpr_log import XprLogger

logger = XprLogger(constants.COMPONENT_NAME, level=logging.INFO)


def get_table_columns(sqlEngine, table):
    """
        Get table columns
    """
    metadata = db.MetaData()
    table_obj = db.Table(
        table, metadata, autoload=True, autoload_with=sqlEngine
    )
    cols = table_obj.columns.keys()
    cols.remove(constants.COL_PK_ID)
    return cols


def get_experiment_id(connection, sqlEngine):
    """
        Get Experiment ID of latest row
    """
    metadata = db.MetaData()
    experiments = db.Table(
        constants.DOE_EXPERIMENTS_TABLE, metadata,
        autoload=True, autoload_with=sqlEngine
    )
    query = db.select([db.func.max(experiments.columns.id)])
    resultproxy = connection.execute(query)
    resultset = resultproxy.fetchall()
    resultproxy.close()
    return resultset[0][0]


def insert_record_in_table(connection, table, row_dict):
    """
        Method to insert rows in given table
    """
    try:
        row_df = pd.DataFrame(row_dict, index=[0])
        row_df.to_sql(
            name=table, con=connection,
            if_exists='append', index=False
        )
    except Exception as e:
        raise e


def compare_columns_in_request(sqlEngine, columns, table):
    """
        Helper method to check columns in request
        against the db schema
    """
    return functools.reduce(
        lambda i, j: i and j,
        map(lambda m, k: m == k, columns,
            get_table_columns(sqlEngine, table)), True)


def check_request_json_format(sqlEngine, request_json):
    """
        Method to valid correct format for request
    """
    try:
        request_experiments_columns = list(request_json)
        request_experiments_columns.remove(
            constants.DOE_FACTORS_TABLE
        )
        experiments_status = compare_columns_in_request(
            sqlEngine, request_experiments_columns,
            constants.DOE_EXPERIMENTS_TABLE
        )

        request_factors_data = request_json[
            constants.DOE_FACTORS_TABLE
        ]
        factors_status_list = [
            compare_columns_in_request(
                sqlEngine, list(row),
                constants.DOE_FACTORS_TABLE
            )
            for row in request_factors_data
        ]
        factors_status = True
        for status in factors_status_list:
            if not status:
                factors_status = False

        if not experiments_status and not factors_status:
            raise KeyError
    except (KeyError, ValueError, IndexError) as e:
        raise e


def check_incoming_request():
    """
        Method to check valid request type
    """
    input_request = None
    try:
        if request.method == constants.REQUEST_METHOD and request.is_json:
            input_request = request.get_json()
    except (ValueError, TypeError, json.JSONDecodeError) as e:
        print(e)
    if not input_request:
        print("ERROR! Your request couldn't be parsed. "
                    "Make sure that it's a valid JSON object.")
    return input_request
