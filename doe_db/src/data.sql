CREATE DATABASE  IF NOT EXISTS `doe`;
USE `doe`;

CREATE TABLE IF NOT EXISTS `experiments`(
        `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `name` TEXT,
        `description` TEXT,
        `inclusion_criteria` TEXT,
        `exclusion_criteria` TEXT
);

CREATE TABLE IF NOT EXISTS `factors`(
        `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `experiment_id` INT(11),
        FOREIGN KEY (experiment_id) REFERENCES experiments(id),
        `name` VARCHAR(100),
        `blocking_factor` BOOLEAN NOT NULL DEFAULT 0,
        `levels` TEXT
);

DELIMITER //

CREATE PROCEDURE create_groups_table(var_id INT)
BEGIN
    SET @query_groups_table = CONCAT('CREATE TABLE IF NOT EXISTS `groups_for_exp_id_',var_id,'`(`id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `description` TEXT);');
    PREPARE STMT FROM @query_groups_table;
    EXECUTE STMT;
    DEALLOCATE PREPARE STMT;
END; //

CREATE PROCEDURE create_participants_table(var_id INT)
BEGIN
    SET @query_participants_table = CONCAT('CREATE TABLE IF NOT EXISTS `participants_in_exp_id_',var_id,'`(`id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `name` VARCHAR(100), `group_id` INT(11), FOREIGN KEY (group_id) REFERENCES groups_for_exp_id_',var_id,'(id), `age` INT(11), `gender` CHAR, `hypertension_present` BOOLEAN NOT NULL DEFAULT 0, `diabetes_present` BOOLEAN NOT NULL DEFAULT 0, `covid19_positive` BOOLEAN NOT NULL DEFAULT 0, `rejection_reason` TEXT);');
    PREPARE STMT FROM @query_participants_table;
    EXECUTE STMT;
    DEALLOCATE PREPARE STMT;
END; //

CREATE PROCEDURE create_data_table(var_id INT)
BEGIN
    SET @query_data_table = CONCAT('CREATE TABLE IF NOT EXISTS `data_for_exp_id_',var_id,'`(`participant_id` INT(11) NOT NULL AUTO_INCREMENT, FOREIGN KEY (participant_id) REFERENCES participants_in_exp_id_',var_id,'(id), `entry_date` DATE, PRIMARY KEY (participant_id, entry_date), `temperature` FLOAT, `bp` TEXT, `weight` INT(11), `covid_positive` BOOLEAN NOT NULL DEFAULT 0);');
    PREPARE STMT FROM @query_data_table;
    EXECUTE STMT;
    DEALLOCATE PREPARE STMT;
END; //

CREATE PROCEDURE create_all_tables()
BEGIN
    DECLARE var_id INT;
    DEClARE curExperiments
		CURSOR FOR
			SELECT id FROM experiments;
    OPEN curExperiments;
    create_tables: LOOP
                   FETCH curExperiments INTO var_id;
                   CALL create_groups_table(var_id);
                   CALL create_participants_table(var_id);
                   CALL create_data_table(var_id);
    END LOOP create_tables;
END; //

DELIMITER ;

SET GLOBAL event_scheduler = ON;

CREATE EVENT create_tables_on_insert_row_in_experiments
ON SCHEDULE EVERY 1 SECOND
DO
    CALL create_all_tables();
